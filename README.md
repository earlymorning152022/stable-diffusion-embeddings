# Stable Diffusion Embeddings

- the number in filename is the number of training steps; you'll probably want to save and use the embedding without it.
- fat embeddings with large number of vectors are best used at the end of prompt, possibly with some `[]` to reduce their effect.
- all the training is done at 512x512; I made unsuccessful attempts to train at 512x768 and 192x192.
- I train using:
   - [web ui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)
   - [nicolai256 repostiory](https://github.com/nicolai256/Stable-textual-inversion_win).
- image augmentations are splitting one tall image into two rectangular ones, and extracting faces from pictures, saving then as additional independed pictures.

## [a1](embeddings/a1-36400.pt)
![](embeddings/a1-36400.png)
16 vector embedding trained on 13 pictures by a1 from danbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of a1-36400
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing, penis, eyelashes, text
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```


## [mignon](embeddings/mignon-62500.pt)
![](embeddings/mignon-62500.png)
16 vector embedding trained on 60 pictures by mignon from danbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of mignon-62500
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing, penis, eyelashes, text
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```


## [haruhisky](embeddings/haruhisky-47000.pt)
![](embeddings/haruhisky-47000.png)
16 vector embedding trained on 57 pictures by haruhisky from geblooru, using Waifu Diffusion 1.2 weights.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of haruhisky-47000
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing, penis, eyelashes, text
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```


## [lasterk](embeddings/lasterk-24500.pt)
![](embeddings/lasterk-24500.png)
16 vector embedding trained on 75 pictures by lasterk from danbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of [[lasterk-24500]]
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing, penis, eyelashes, text
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 3017606331, Size: 512x768, Model hash: 45dee52b
```


## [kaoming](embeddings/kaoming-42600.pt)
![](embeddings/kaoming-42600.png)
16 vector embedding trained on 71 pictures by kaoming from kemono party, using Waifu Diffusion 1.2 weights.
```
a painting of a ((nude)) ((red-haired)) girl frowning in a straw hat on a (farm), in style of kaoming-42600
Negative prompt: (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 12, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```

## [birgbear](embeddings/birgbear-100000.pt)
![](embeddings/birgbear-100000.png)
16 vector embedding trained on 74 (114 augmented) pictures by birgbear from gelbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a ((nude)) ((red-haired)) girl in a ((straw hat)) on a (farm), in style of embeddings_gs-100000
Negative prompt: two people, ((clothes)), (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 10, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```


## [geewhy](embeddings/geewhy-60000.pt)
![](embeddings/geewhy-60000.png)
16 vector embedding trained on 59 (81 augmented) pictures by geewhy from danbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a nude red-haired girl in a ((straw hat)) on a ((farm)), in style of geewhy-60000
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```

## [Satoshi Urushihara](embeddings/satoshi-urushihara-50000.pt)
![](embeddings/satoshi-urushihara-50000.png)
16 vector embedding trained on 92 (190 augmented) pictures by Satoshi Urushihara from danbooru, using Waifu Diffusion 1.2 weights.
```
a painting of a nude red-haired girl in a ((straw hat)) on a ((farm)), in style of embeddings_gs-50000
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 5, Seed: 398300211, Size: 512x512, Model hash: 45dee52b
```

## [camonome](embeddings/camonome-55500.pt)
![](embeddings/camonome-55500.png)
16 vector embedding trained on 116 (223 augmented) pictures by camonome from kemono party, using Waifu Diffusion 1.2 weights.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of [[camonome-55500]]
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 3017606331, Size: 512x768, Model hash: 45dee52b
```

## [camonome 2](embeddings/cam-100000.pt)
![](embeddings/cam-100000.png)
16 vector embedding trained on 116 (223 augmented) pictures by camonome from kemono party, using Waifu Diffusion 1.2 weights.
This embeddnig was trained using the alternate implementation of textual inversion by me that I will be building into
the UI.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of [[camonome-55500]]
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 3017606331, Size: 512x768, Model hash: 45dee52b
```

## [mana](embeddings/mana-100000.pt)
![](embeddings/mana-100000.png)
16 vector embedding trained on 51 (108 augmented) pictures by mana from sad panda, using Waifu Diffusion 1.2 weights.
```
a nude ((red-haired)) girl in a ((straw hat)) standing, ((farm)), daytime, field, sky, clouds, sun, art by mana-100000
Negative prompt: multiple people, crowd, (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 3017606331, Size: 768x768, Model hash: 45dee52b, Denoising strength: 0.69
```

## [as109](embeddings/as109-60000.pt)
![](embeddings/as109-60000.png)
16 vector embedding trained on 42 (75 augmented) pictures by as109 from gelbooru, using Waifu Diffusion 1.2 weights.
```
a ((nude)) red-haired GROWNUP ADULT WOMAN WITH GIGANTIC BREASTS in a ((straw hat)) on a ((farm)), daytime, field, sky, clouds, sun, in style of embeddings_gs-60000
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 3679341286, Size: 512x512, Model hash: 45dee52b
```

## [gweda](embeddings/gweda-76000.pt)
![](embeddings/gweda-76000.png)
16 vector embedding trained on 96 (252 augmented) pictures by gweda from sad panda, using Waifu Diffusion 1.2 weights.
```
a painting of a nude red-haired girl with ((raised hands)) in a straw hat on a farm, in style of embeddings_gs-76000
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 12, Seed: 1255151833, Size: 960x960, Model hash: 45dee52b, Denoising strength: 0.5
```

## [Da Mao Banlangen](embeddings/Da-Mao-Banlangen-76500.pt)
![](embeddings/Da-Mao-Banlangen-76500.png)
16 vector embedding trained on 29 (57 augmented) pictures by Da Mao Banlangen from sad panda, using Waifu Diffusion 1.2 weights.
```
a painting of a ((nude)) red-haired girl wearing a ((straw hat)) on a farm, art by Da Mao Banlangen-76500
Negative prompt: (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, (extra_limb), (poorly drawn hands)
Steps: 20, Sampler: Euler a, CFG scale: 12, Seed: 1255151833, Size: 512x512, Model hash: 45dee52b
```



## [Kase Daiki](embeddings/kase-daiki-46506.pt)
![](embeddings/kase-daiki-46506.png)
16 vector embedding trained on 82 (200 augmented) pictures by Kase Daiki from sad panda, using Waifu Diffusion 1.2 weights and web ui.
```
a painting of a (((nude))) red-haired girl wearing a ((straw hat)) on a farm, day, sky, clouds, art by (kase-daiki:0.8)
Negative prompt: open mouth, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, (extra_limb), (poorly drawn hands)
Steps: 20, Sampler: Euler a, CFG scale: 9, Seed: 1255151833, Size: 512x640, Model hash: 45dee52b
```


## [taiga](embeddings/taiga-38567.pt)
![](embeddings/taiga-38567.png)
16 vector embedding trained on 90 (360 augmented) pictures by Taiga from sad panda, using Waifu Diffusion 1.2 weights and web ui.
```
a painting of a (((nude))) red-haired girl wearing a ((straw hat)) on a farm, art by taiga
Negative prompt: open mouth, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, (extra_limb), (poorly drawn hands)
Steps: 20, Sampler: Euler a, CFG scale: 5, Seed: 1255151833, Size: 768x768, Model hash: 45dee52b, Denoising strength: 0.7
```


## [cutesexyrobutts](embeddings/cutesexyrobutts-100000.pt)
![](embeddings/cutesexyrobutts-100000.png)
16 vector embedding trained on 42 (93 augmented) pictures by cutesexyrobutts from sad panda, using Waifu Diffusion 1.2 weights and web ui.
```
a painting of a (((nude))) red-haired girl in a straw hat on a farm, in style of cutesexyrobutts
Negative prompt: (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7.5, Seed: 3017606331, Size: 768x768, Model hash: 45dee52b, Denoising strength: 0.5
```


## [agm](embeddings/agm-60000.pt)
![](embeddings/agm-60000.png)
16 vector embedding trained on 92 (171 augmented) pictures by agm from sad panda, using Waifu Diffusion 1.2 weights and web ui.
```
a ((nude)) girl with long red hair wearing choker and ((jewelry)) ((raising hands)) on a (farm), detailed face, detailed eyes, art by (agm-60000:0.95)
Negative prompt: blurry, messy
Steps: 20, Sampler: Euler a, CFG scale: 11, Seed: 438818373, Size: 768x768, Model hash: 45dee52b, Denoising strength: 0.5
```


## [Amane Kanata](embeddings/Amane-Kanata-40500.pt)
![](embeddings/Amane-Kanata-40500.png)
8 vector embedding trained on 63 (133 augmented) pictures of a fictional character going by the name of Amane Kanata from gelbooru, using Waifu Diffusion 1.2 weights.
```
portrait of a girl with wings and hair decoration, amane kanata
Negative prompt: (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 4.5, Seed: 2370720895, Size: 512x640, Model hash: 45dee52b
```


## [Amane Kanata 2](embeddings/amane-kanata2-42198.pt)
![](embeddings/amane-kanata2-42198.png)
8 vector embedding trained on same dataset as above, using Waifu Diffusion 1.2 weights and web ui.
```
a photo of a smiling girl, amane-kanata in (white and blue winter jacket)
Negative prompt: open mouth, (ugly), fat, obese, (((deformed))), [blurry], bad anatomy, disfigured, poorly drawn face, mutation, furry, mutated, (extra_limb), (poorly drawn hands), messy drawing
Steps: 20, Sampler: Euler a, CFG scale: 7, Seed: 2370720895, Size: 512x640, Model hash: 45dee52b
```


## [Kazama Iroha](embeddings/kazama-iroha-15895.pt)
![](embeddings/kazama-iroha-15895.png)
8 vector embedding trained on 105 (230 augmented) pictures of a fictional character going by the name of Kazama Iroha from gelbooru, using Waifu Diffusion 1.2 weights and web ui.
```
a photo of frowning angry kazama-iroha wearing a kimono and a ((katana sword)) in a (shinto shrine)
Negative prompt: disfigured, mutation, furry, mutated, extra_limb, poorly, messy
Steps: 20, Sampler: Euler a, CFG scale: 10, Seed: 3017606331, Size: 640x1024, Model hash: 45dee52b, Batch size: 2, Batch pos: 0, Denoising strength: 0.7
```
